﻿using System;

using Xamarin.Forms;

namespace CS481HW6
{
    public class DetailsPage : ContentPage
    {
        public DetailsPage()
        {
            Content = new StackLayout
            {
                Children = {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}

