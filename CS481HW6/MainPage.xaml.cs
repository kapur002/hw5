﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace CS481HW6
{
    public partial class MainPage : ContentPage
    {
        //create a List of ProductData JSON and name it items
        public List<ProductData> items;

        //store products.json in a string called fileName
        public string fileName = "products.json";

        public MainPage()
        {
            InitializeComponent();
            //call the LoadJson function
            readJsonFile();
        }

        //function to read the Json file
        public void readJsonFile()
        {
            using (StreamReader r = new StreamReader(fileName))
            {

                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<ProductData>>(json);
            }
            customProductsCell.ItemsSource = items;
        }

        //function for info button
        private async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as ProductData;
            await Navigation.PushAsync(new DetailPage(itemClicked));
        }

    }
}
