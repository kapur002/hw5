﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CS481HW6
{
    public partial class DetailPage : ContentPage
    {
        public DetailPage(ProductData currentItem)
        {
            InitializeComponent();
            productName.Text = "Item Name: " + currentItem.ProductName;
            productImg.Source = currentItem.ImageUrl;
            productDescr.Text = "Item Description: " + currentItem.Description;
            productPrice.Text = "Price: $" + currentItem.Price;
            productRating.Text = "Rating: " + currentItem.StarRating;

        }
    }
}
